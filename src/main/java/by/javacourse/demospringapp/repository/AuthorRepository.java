package by.javacourse.demospringapp.repository;

import by.javacourse.demospringapp.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AuthorRepository extends JpaRepository<Author,Long> {
}
