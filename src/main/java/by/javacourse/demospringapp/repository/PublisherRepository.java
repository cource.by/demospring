package by.javacourse.demospringapp.repository;


import by.javacourse.demospringapp.model.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PublisherRepository extends JpaRepository<Publisher,Long> {
}
