package by.javacourse.demospringapp.repository;

import by.javacourse.demospringapp.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book,Long> {
}
