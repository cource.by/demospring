package by.javacourse.demospringapp.bootstrap;

import by.javacourse.demospringapp.model.Author;
import by.javacourse.demospringapp.model.Book;
import by.javacourse.demospringapp.model.Publisher;
import by.javacourse.demospringapp.repository.AuthorRepository;
import by.javacourse.demospringapp.repository.BookRepository;
import by.javacourse.demospringapp.repository.PublisherRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;


import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


@Slf4j
@Component
@RequiredArgsConstructor
public class BootstrapData implements CommandLineRunner {
private final AuthorRepository authorRepository;
private  final BookRepository bookRepository;
private  final PublisherRepository publisherRepository;
    @Override
    public void run(String... args) {
        Author author = Author.builder().firstName("John").lastName("Tolkien").build();
        Author author2 = Author.builder().firstName("Johnny").lastName("Tolkien").build();
        Set<Author> authorSet = new HashSet<>();
        authorSet.add(author);
        authorSet.add(author2);
        Publisher publisher = Publisher.builder().name("RusFond").build();
        Publisher publisher2 = Publisher.builder().name("TimeDay").build();
        Book book1 = Book.builder().name("Hobbit").publishYear(1953).
                author(Collections.singleton(author)).publisher(publisher).build();
        Book book2 = Book.builder().name("Lord of the Ring").publishYear(1954).
                author(Collections.singleton(author)).publisher(publisher).build();
        Book book3 = Book.builder().name("Silmarillion").publishYear(1956).
                author(authorSet).publisher(publisher2).build();
        Set<Book> bookSet = new HashSet<>();

        bookSet.add(book1);
        bookSet.add(book2);
        bookSet.add(book3);

        Set<Book> bookSetForPublisher1 = new HashSet<>();
        Set<Book> bookSetForPublisher2 = new HashSet<>();

        bookSetForPublisher1.add(book1);
        bookSetForPublisher1.add(book2);
        author.setBooks(bookSet);
        author2.setBooks(Collections.singleton(book3));
        publisher.setBooks(bookSetForPublisher1);

        bookSetForPublisher2.add(book3);
        publisher2.setBooks(bookSetForPublisher2);

        authorRepository.save(author);
        authorRepository.save(author2);
        publisherRepository.save(publisher);
        publisherRepository.save(publisher2);
        bookRepository.save(book1);
        bookRepository.save(book2);
        bookRepository.save(book3);

        log.info("Author count in DB: {}",authorRepository.count());
        log.info("Book count in DB: {}",bookRepository.count());
        log.info("Publisher count in DB: {}",publisherRepository.count());
    }
}
